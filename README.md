# Akku.scm

[![pipeline status](https://gitlab.com/akkuscm/akku/badges/master/pipeline.svg)](https://gitlab.com/akkuscm/akku/commits/master)

[Akku.scm](https://akkuscm.org/) is a language package manager for
Scheme. It grabs hold of code and vigorously shakes it until it
behaves properly.

* No complicated setup to point out where libraries are; Akku finds
  them!
* Separately declare dependencies and locked versions for your
  project.
* Convert R7RS libraries for use with Chez Scheme and other R6RS
  Scheme implementations.
* Numerous R6RS [packages][packages], as well as R7RS libraries
  mirrored from [Snow][snow].

 [packages]: https://akkuscm.org/packages/
 [snow]: https://snow-fort.org/

## Dependencies

Akku requires curl and optionally git. It has been tested on
GNU/Linux, macOS, FreeBSD, Cygwin, OpenBSD and MSYS2. Although it
supports many Scheme implementations, Akku itself currently requires
either GNU Guile, Chez Scheme or Loko Scheme.

## Installation

There are these options:

 - Use the release source tarball. This option uses the GNU build
   system and requires Guile 2.2 or 3.0 (the development package),
   pkg-config, make, git and curl. This works on the widest range
   of operating systems and architectures. Unpack the tarball, run
   `./configure`, `make` and `sudo make install`. If the configure
   script can't find Guile then you might need something like this:
   `./configure GUILD=/usr/bin/guild GUILE_CONFIG=/usr/bin/guile-config`.

 - Use the pre-built version from [GitLab][GitLabTags]. Pre-built
   versions based on Chez Scheme are available for GNU/Linux amd64.
   Use the file ending with src.tar.xz for other architectures.
   This type of installation uses `~/.local`.

 - If you would like to install directly from Git, then
   see [CONTRIBUTING](CONTRIBUTING.md) for instructions. Running
   `bin/akku.sps` directly is not going to work.

Please remember to verify the OpenPGP signatures. The releases are
signed with [E33E61A2E9B8C3A2][key].

 [GitLabTags]: https://gitlab.com/akkuscm/akku/tags
 [key]: https://nm.debian.org/person/weinholt/

## Usage

How to get started with a new project:

 - Run `akku init project-name`. This creates a new project from a
   template. You can also safely run akku in your existing project
   directory.
 - Run `akku list` to list available packages (`akku update` downloads
   the latest package index).
 - Run `akku install <pkg>` to install a named package. This also
   installs the code in your current directory into `.akku`. If you
   add new source code files you'll need to rerun `akku install`.
 - Run `.akku/env` to get a shell in an environment that uses the
   programs and libraries in `.akku`.

The installed libraries should now be in the load path of Chez Scheme,
GNU Guile (with R6RS settings), Ikarus, Larceny, Mosh, Racket
(plt-r6rs), Sagittarius, Vicare and Ypsilon. Original R7RS libraries
will be available to Chibi, Gauche, Larceny, Loko, Sagittarius and
Ypsilon. Any installed programs are available in your shell's path.

Most implementations can use the libraries as-is. GNU Guile 3.0
requires the `--r6rs` command line flag.

Your users can unpack your source code and run `akku install` to get
the same dependencies that you used during development.

When you have a new package you want to make available to others, you
can publish it with `akku publish`.

More details are in [akku.1](https://akkuscm.org/docs/manpage.html) manpage.

## Docker image

The [akkuscm/akku](https://hub.docker.com/r/akkuscm/akku) image is
updated to stay relatively up to date with git. It is a stripped down
build based on Alpine Linux. Here's a simple way to use this image to
run tests with GitLab CI:

```yaml
image: "akkuscm/akku:latest"

build:
  before_script:
    - akku install
  script:
    - .akku/env ./run-tests.sh
```

Debian-based images are available for a few Scheme implementations.
See Docker hub for a list of tags. They come prepared with a
`scheme-script` wrapper that works as described in the R6RS
non-normative appendix.

## Contact

* [Akku on GitLab issues](https://gitlab.com/akkuscm/akku/issues).
* The IRC channel `#akku` on [Libera.Chat](https://libera.chat/), but
  `#scheme` also works if the subject is about Scheme in general.
* The [Akku](https://akkuscm.org/) website.
* Group discussions can be had via the Usenet group comp.lang.scheme,
  available through any Usenet provider,
  e.g. [Eternal September](https://www.eternal-september.org/).
* The `#akku` channel on the Scheme Discord server. You should be able
  to find a current invite at [Akku](https://akkuscm.org/).

## License

Akku.scm is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

### Clarification on the scope of the license, etc

The license above covers the source code of Akku.scm itself. Mere use
of Akku.scm as a tool does not place any restrictions on your source
code. You may view Akku.scm as any other program that helps with a
build process, such as CMake or GNU automake. These tools generate
files that aid in the build process, but their licenses do not carry
over to the projects where the tools are used.

To make this point extra clear, consider the following scenario. Run
`akku install` in your project to get all dependencies installed into
the `.akku` directory. Now take your project directory over to a
computer where Akku.scm is not installed at all. You should still be
able to build your project, even though Akku.scm's source code is not
on that computer.

If licensing is important to you then you should review the license of
any packages that you install into your project. Some effort is made
to summarize the license information on each package into a single
SPDX license expression when packages are uploaded to the index, but
this information may be wrong or incomplete.

Akku.scm also uses simplistic heuristics to gather copyright notices
and licenses into `.akku/notices`. Most licenses require some form of
attribution when code is distributed in binary form and this directory
can be a good start.
